package org.banking.bankalot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankalotApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankalotApplication.class, args);
	}

}

