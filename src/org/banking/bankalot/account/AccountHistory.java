package org.banking.bankalot.account;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class AccountHistory {

    @JsonProperty
    private String transactionDateTime;
    @JsonProperty
    private String currency;
    @JsonProperty
    private BigDecimal cashMovement;


    void setTransactionDateTime(String transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    public String getTransactionDateTime() {
        return transactionDateTime;
    }

    void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    void setCashMovement(BigDecimal cashMovement) {
        this.cashMovement = cashMovement;
    }

    public BigDecimal getCashMovement() {
        return cashMovement;
    }
}
