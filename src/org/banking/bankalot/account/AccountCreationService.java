package org.banking.bankalot.account;

import org.joda.money.CurrencyUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class AccountCreationService {

    private BankAccountDao bankAccountDao;

    @Autowired
    AccountCreationService(BankAccountDao bankAccountDao) {
        this.bankAccountDao = bankAccountDao;
    }


    /**
     * Account will be created with a zero starting balance in the currency.
     *
     * @param currency the currency the account will be in
     * @return unique account number
     * @throws IllegalArgumentException if the account number already exists
     */
    @Transactional
    public String createAccount(CurrencyUnit currency) {
        return bankAccountDao.save(BankAccount.newDefaultInstance(currency)).getAccountNumber();
    }
}
