package org.banking.bankalot.account;

import org.joda.money.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.transaction.Transactional;
import java.util.Optional;

@Component
public class BankWithdrawService {

    private EntityManager entityManager;

    @Autowired
    BankWithdrawService(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * @param accountNumber the account to withdraw money from
     * @param withdrawAmount the amount to withdraw
     * @throws IllegalArgumentException if the accountNumber is not found
     */
    @Transactional
    public void withdraw(String accountNumber, Money withdrawAmount) {
        BankAccount bankAccount = Optional.ofNullable(entityManager.find(BankAccount.class, accountNumber))
                .orElseThrow(() -> new IllegalArgumentException("Account not found: " + accountNumber));
        entityManager.lock(bankAccount, LockModeType.OPTIMISTIC);
        bankAccount.withdraw(withdrawAmount);
    }
}
