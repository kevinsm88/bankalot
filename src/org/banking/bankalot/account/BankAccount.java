package org.banking.bankalot.account;

import org.hibernate.annotations.GenericGenerator;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

@Entity
class BankAccount {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String accountNumber;

    // using the JPA @Version mechanism for optimistic locking of the records
    // refer to the below post regarding reasons for using this mechanism
    // https://www.developer.com/java/manage-concurrent-access-to-jpa-entity-with-locking.html
    @Version
    private int version;

    private String currency;

    private BigDecimal balance;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER) // EAGER due to not being able to add when in LAZY mode
    @JoinColumn(name = "bankAccountId")
    private Collection<BankAccountHistory> bankAccountHistories = new ArrayList<>();

    static BankAccount newDefaultInstance(CurrencyUnit currency) {
        BankAccount bankAccount = new BankAccount();
        bankAccount.currency = currency.getCode();
        bankAccount.balance = BigDecimal.valueOf(0d);
        return bankAccount;
    }

    int getVersion() {
        return version;
    }

    void setVersion(int version) {
        this.version = version;
    }

    String getAccountNumber() {
        return accountNumber;
    }

    void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    String getCurrency() {
        return currency;
    }

    void setCurrency(String currency) {
        this.currency = currency;
    }

    BigDecimal getBalance() {
        return balance;
    }

    void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    Collection<BankAccountHistory> getBankAccountHistories() {
        return Collections.unmodifiableCollection(bankAccountHistories);
    }

    public void setBankAccountHistories(Collection<BankAccountHistory> bankAccountHistories) {
        this.bankAccountHistories = bankAccountHistories;
    }

    Money getMoneyBalance() {
        return Money.of(CurrencyUnit.of(currency), balance);
    }

    void deposit(Money depositAmount) {
        balance = balance.add(depositAmount.getAmount());
        addTransactionHistory(depositAmount);
    }

    void withdraw(Money withdrawAmount) {
        if (Money.of(CurrencyUnit.of(currency), balance).isLessThan(withdrawAmount)) {
            throw new IllegalArgumentException("Insufficient funds.");
        }
        balance = balance.subtract(withdrawAmount.getAmount());
        addTransactionHistory(withdrawAmount.negated());
    }

    private void addTransactionHistory(Money cashMovement) {
        BankAccountHistory bankAccountHistory = new BankAccountHistory();
        bankAccountHistory.setBankAccount(this);
        bankAccountHistory.setCashMovement(cashMovement.getAmount());
        bankAccountHistory.setCurrency(cashMovement.getCurrencyUnit().getCode());
        bankAccountHistory.setTransactionDateTime(LocalDateTime.now());
        bankAccountHistories.add(bankAccountHistory);
    }
}
