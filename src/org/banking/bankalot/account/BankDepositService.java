package org.banking.bankalot.account;

import org.joda.money.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.transaction.Transactional;
import java.util.Optional;

@Component
public class BankDepositService {

    private EntityManager entityManager;

    @Autowired
    BankDepositService(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * @param accountNumber the account to deposit money into
     * @param depositAmount the amount to deposit
     * @throws IllegalArgumentException if the accountNumber is not found
     */
    @Transactional
    public void deposit(String accountNumber, Money depositAmount) {
        BankAccount bankAccount = Optional.ofNullable(entityManager.find(BankAccount.class, accountNumber))
                .orElseThrow(() -> new IllegalArgumentException("Account not found: " + accountNumber));
        entityManager.lock(bankAccount, LockModeType.OPTIMISTIC);
        bankAccount.deposit(depositAmount);
    }
}
