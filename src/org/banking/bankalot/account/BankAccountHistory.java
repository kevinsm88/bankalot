package org.banking.bankalot.account;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
class BankAccountHistory {
    
    @Id
    @GeneratedValue
    private long id;
    
    private LocalDateTime transactionDateTime;
    
    private String currency;
    
    private BigDecimal cashMovement;
    
    @ManyToOne
    @JoinColumn(name = "bankAccountId")
    private BankAccount bankAccount;

    long getId() {
        return id;
    }

    void setId(long id) {
        this.id = id;
    }

    LocalDateTime getTransactionDateTime() {
        return transactionDateTime;
    }

    void setTransactionDateTime(LocalDateTime transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    String getCurrency() {
        return currency;
    }

    void setCurrency(String currency) {
        this.currency = currency;
    }

    void setCashMovement(BigDecimal cashMovement) {
        this.cashMovement = cashMovement;
    }

    BankAccount getBankAccount() {
        return bankAccount;
    }

    void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    BigDecimal getCashMovement() {
        return cashMovement;
    }
}
