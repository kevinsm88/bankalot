package org.banking.bankalot.account;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.Collection;

public class AccountStatement {

    @JsonProperty
    private String currency;
    @JsonProperty
    private BigDecimal balance;
    @JsonProperty
    private Collection<AccountHistory> accountHistories;

    void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    void setAccountHistories(Collection<AccountHistory> accountHistories) {
        this.accountHistories = accountHistories;
    }

    public Collection<AccountHistory> getAccountHistories() {
        return accountHistories;
    }
}
