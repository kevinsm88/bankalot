package org.banking.bankalot.account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface BankAccountDao extends JpaRepository<BankAccount, String> {

}
