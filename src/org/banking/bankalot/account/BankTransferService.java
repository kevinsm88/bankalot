package org.banking.bankalot.account;

import org.joda.money.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class BankTransferService {

    private BankWithdrawService bankWithdrawService;
    private BankDepositService bankDepositService;

    @Autowired
    BankTransferService(BankWithdrawService bankWithdrawService, BankDepositService bankDepositService) {
        this.bankWithdrawService = bankWithdrawService;
        this.bankDepositService = bankDepositService;
    }


    /**
     * @param fromAccountNumber the account to transfer from
     * @param toAccountNumber the account to transfer to
     * @param transferAmount the amount to transfer
     * @throws IllegalArgumentException if either of the accounts are not valid or if there is no
     */
    @Transactional
    public void transfer(String fromAccountNumber, String toAccountNumber, Money transferAmount) {
        bankWithdrawService.withdraw(fromAccountNumber, transferAmount);
        bankDepositService.deposit(toAccountNumber, transferAmount);
    }
}
