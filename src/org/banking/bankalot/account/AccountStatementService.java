package org.banking.bankalot.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

@Component
public class AccountStatementService {

    private BankAccountDao bankAccountDao;

    @Autowired
    AccountStatementService(BankAccountDao bankAccountDao) {
        this.bankAccountDao = bankAccountDao;
    }


    /**
     * @return the {@link AccountStatement} for the account number
     */
    public AccountStatement accountStatement(String accountNumber) {
        BankAccount bankAccount = bankAccountDao.findById(accountNumber)
                .orElseThrow(() -> new IllegalArgumentException("From account does not exist: " + accountNumber));
        return new AccountStatement() {{
            setCurrency(bankAccount.getCurrency());
            setBalance(bankAccount.getBalance());
            setAccountHistories(bankAccount.getBankAccountHistories().stream().map(history -> new AccountHistory() {{
                setCashMovement(history.getCashMovement());
                setCurrency(history.getCurrency());
                setTransactionDateTime(history.getTransactionDateTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
            }}).collect(Collectors.toList()));
        }};
    }
}
