package org.banking.bankalot.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

class CurrencyBean {
    @JsonProperty
    String currency;
}
