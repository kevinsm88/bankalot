package org.banking.bankalot.rest;

import org.joda.money.IllegalCurrencyException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
class BadRequestHandler {

    @ResponseBody
    @ExceptionHandler({IllegalArgumentException.class, IllegalCurrencyException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String handleKnownExceptionsResultingFromBadRequests(RuntimeException exception) {
        return exception.getLocalizedMessage();
    }
}
