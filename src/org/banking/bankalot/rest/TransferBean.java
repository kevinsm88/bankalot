package org.banking.bankalot.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

class TransferBean {
    @JsonProperty
    String fromAccountNumber;
    @JsonProperty
    String toAccountNumber;
    @JsonProperty
    String currency;
    @JsonProperty
    BigDecimal amount;
}
