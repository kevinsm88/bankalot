package org.banking.bankalot.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

class AccountStatementBean {
    @JsonProperty
    String accountNumber;
}
