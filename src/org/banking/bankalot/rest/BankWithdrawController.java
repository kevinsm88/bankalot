package org.banking.bankalot.rest;

import org.banking.bankalot.account.BankDepositService;
import org.banking.bankalot.account.BankWithdrawService;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BankWithdrawController {

    private BankWithdrawService bankWithdrawService;

    @Autowired
    BankWithdrawController(BankWithdrawService bankWithdrawService) {
        this.bankWithdrawService = bankWithdrawService;
    }


    @PostMapping(value = "/withdraw", produces = "application/json", consumes = "application/json")
    public void withdraw(@RequestBody WithdrawBean withdrawDetails) {
        CurrencyUnit currency = CurrencyUnit.of(withdrawDetails.currency);
        Money withdrawAmount = Money.of(currency, withdrawDetails.amount);
        bankWithdrawService.withdraw(withdrawDetails.accountNumber, withdrawAmount);
    }
}
