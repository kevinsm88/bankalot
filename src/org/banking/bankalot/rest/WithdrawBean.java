package org.banking.bankalot.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

class WithdrawBean {
    @JsonProperty
    String accountNumber;
    @JsonProperty
    String currency;
    @JsonProperty
    BigDecimal amount;
}
