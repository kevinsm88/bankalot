package org.banking.bankalot.rest;

import org.banking.bankalot.account.AccountStatement;
import org.banking.bankalot.account.AccountStatementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountStatementController {

    private AccountStatementService accountStatementService;

    @Autowired
    AccountStatementController(AccountStatementService accountStatementService) {
        this.accountStatementService = accountStatementService;
    }


    @GetMapping(name = "/account", produces = "application/json", consumes = "application/json")
    public AccountStatement getAccountStatement(@RequestBody AccountStatementBean accountStatementBean) {
        return accountStatementService.accountStatement(accountStatementBean.accountNumber);
    }
}
