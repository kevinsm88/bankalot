package org.banking.bankalot.rest;

import org.banking.bankalot.account.BankTransferService;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BankTransferController {

    private BankTransferService bankTransferService;

    @Autowired
    BankTransferController(BankTransferService bankTransferService) {
        this.bankTransferService = bankTransferService;
    }


    @PostMapping(value = "/transfer", produces = "application/json", consumes = "application/json")
    public void transfer(@RequestBody TransferBean transferBean) {
        CurrencyUnit currency = CurrencyUnit.of(transferBean.currency);
        Money transferAmount = Money.of(currency, transferBean.amount);
        bankTransferService.transfer(transferBean.fromAccountNumber, transferBean.toAccountNumber, transferAmount);
    }
}
