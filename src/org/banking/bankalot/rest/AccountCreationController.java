package org.banking.bankalot.rest;

import org.banking.bankalot.account.AccountCreationService;
import org.joda.money.CurrencyUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class AccountCreationController {

    private AccountCreationService accountCreationService;

    @Autowired
    AccountCreationController(AccountCreationService accountCreationService) {
        this.accountCreationService = accountCreationService;
    }

    @PostMapping(value = "/account", produces = "application/json", consumes = "application/json")
    public String newAccount(@RequestBody CurrencyBean currencyBean) {
        CurrencyUnit currency = CurrencyUnit.of(currencyBean.currency);
        return accountCreationService.createAccount(currency);
    }
}
