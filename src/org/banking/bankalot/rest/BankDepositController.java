package org.banking.bankalot.rest;

import org.banking.bankalot.account.BankDepositService;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BankDepositController {

    private BankDepositService bankDepositService;

    @Autowired
    BankDepositController(BankDepositService bankDepositService) {
        this.bankDepositService = bankDepositService;
    }


    @PostMapping(value = "/deposit", produces = "application/json", consumes = "application/json")
    public void deposit(@RequestBody DepositBean depositDetails) {
        CurrencyUnit currency = CurrencyUnit.of(depositDetails.currency);
        Money depositAmount = Money.of(currency, depositDetails.amount);
        bankDepositService.deposit(depositDetails.accountNumber, depositAmount);
    }
}
