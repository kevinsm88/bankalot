Bankalot
========
This a standalone HTTP/JSON API providing simple functionality for transferring money between bank accounts.

It has functionality to:  
	- create a new account  
	- deposit money into a specified account  
	- withdraw money from a specified account  
	- transfer money between two accounts  
	- fetch the balances and transaction history of a specified account

Bank transfer test
==================
TestAccountStatementController performs a full end-to-end test of:  
	- Creating two accounts  
	- Deposits an initial balance into each of the accounts  
	- Transfer an amount from one account to the other  
	- Retrieve the account statements for each of the accounts  
	- Verifies the balances are correct  
	- Verifies the transaction cash movements are correct  

How to build the project
========================

1. Run ```mvnw package``` from the project directory.  
2. Run ```mvnw spring-boot:run``` from the project directory to launch the Bankalot app.  