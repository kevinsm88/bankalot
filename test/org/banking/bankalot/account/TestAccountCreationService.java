package org.banking.bankalot.account;

import org.joda.money.CurrencyUnit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestAccountCreationService {

    @Autowired
    private BankAccountDao bankAccountDao;

    @Test
    public void testCreateBankAccount() {
        AccountCreationService service = new AccountCreationService(bankAccountDao);
        String accountNumber = service.createAccount(CurrencyUnit.GBP);
        assertTrue("New bank account should be retrieved", bankAccountDao.findById(accountNumber).isPresent());
    }
}
