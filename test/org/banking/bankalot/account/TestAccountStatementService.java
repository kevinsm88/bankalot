package org.banking.bankalot.account;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestAccountStatementService {

    @Autowired
    private AccountStatementService accountStatementService;

    @Autowired
    private BankTransferService bankTransferService;

    @Autowired
    private AccountCreationService accountCreationService;

    @Autowired
    private BankDepositService bankDepositService;


    @Test
    public void testAccountStatement() {
        Money startingBalance = Money.of(CurrencyUnit.GBP, BigDecimal.valueOf(756d));
        String accountNumber = createAccount(startingBalance);
        String anotherAccount = createAccount(startingBalance);
        Money transferAmount = Money.of(CurrencyUnit.GBP, BigDecimal.valueOf(123d));
        transfer(accountNumber, anotherAccount, transferAmount);

        AccountStatement accountStatement = accountStatementService.accountStatement(accountNumber);

        assertEquals("Bank account balance should be £633", startingBalance.minus(transferAmount).getAmount(), accountStatement.getBalance());
        assertEquals("There should be 2 transactions associated with this account", 2, accountStatement.getAccountHistories().size());

        Iterator<AccountHistory> iterator = accountStatement.getAccountHistories().iterator();
        assertEquals("First cash movement should be £756", startingBalance.getAmount(), iterator.next().getCashMovement());
        assertEquals("Second cash movement should be -£123", transferAmount.negated().getAmount(), iterator.next().getCashMovement());
    }


    private String createAccount(Money startingBalance) {
        String accountNumber = accountCreationService.createAccount(CurrencyUnit.GBP);
        bankDepositService.deposit(accountNumber, startingBalance);
        return accountNumber;
    }

    private void transfer(String accountNumber, String anotherAccount, Money transferAmount) {
        bankTransferService.transfer(accountNumber, anotherAccount, transferAmount);
    }
}
