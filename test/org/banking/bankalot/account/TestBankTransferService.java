package org.banking.bankalot.account;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestBankTransferService {

    @Autowired
    private BankAccountDao bankAccountDao;

    @Autowired
    private BankWithdrawService bankWithdrawService;

    @Autowired
    private BankDepositService bankDepositService;

    @Autowired
    private AccountCreationService accountCreationService;

    @Autowired
    private BankTransferService bankTransferService;

    @Test
    public void testTransfer() {
        Money fromAccountStartingBalance = Money.of(CurrencyUnit.GBP, BigDecimal.valueOf(500d));
        String fromAccountNumber = createAccount(fromAccountStartingBalance);

        Money toAccountStartingBalance = Money.of(CurrencyUnit.GBP, BigDecimal.valueOf(250d));
        String toAccountNumber = createAccount(toAccountStartingBalance);

        Money transferAmount = Money.of(CurrencyUnit.GBP, BigDecimal.valueOf(100d));
        bankTransferService.transfer(fromAccountNumber, toAccountNumber, transferAmount);

        assertEquals("From account should have a £400 balance", fromAccountStartingBalance.minus(transferAmount), getBankBalanceFromDb(fromAccountNumber));
        assertEquals("To account should have a £350 balance", toAccountStartingBalance.plus(transferAmount), getBankBalanceFromDb(toAccountNumber));
    }


    private String createAccount(Money startingBalance) {
        String accountNumber = accountCreationService.createAccount(CurrencyUnit.GBP);
        bankDepositService.deposit(accountNumber, startingBalance);
        return accountNumber;
    }

    private Money getBankBalanceFromDb(String accountNumber) {
        return bankAccountDao.findById(accountNumber).orElseThrow(() -> new IllegalStateException("Expected account to be found.")).getMoneyBalance();
    }
}
