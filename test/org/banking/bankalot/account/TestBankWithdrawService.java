package org.banking.bankalot.account;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestBankWithdrawService {

    @Autowired
    private BankAccountDao bankAccountDao;

    @Autowired
    private AccountCreationService accountCreationService;

    @Autowired
    private BankDepositService bankDepositService;

    @Autowired
    private BankWithdrawService bankWithdrawService;

    @Test
    @Transactional // the lock on the withdraw is causing a "no transaction in progress" to be raised
                   // there is an @Transactional on BankWithdrawService#withdraw(...)
                   // I would like to fix this but running out of time - wrapping the test in a single @Transactional for now
                   // TODO this needs to be fixed
    public void testWithdraw() {
        Money startBalance = Money.of(CurrencyUnit.GBP, BigDecimal.valueOf(500d));
        String accountNumber = createAccount(startBalance);
        Money withdrawAmount = Money.of(CurrencyUnit.GBP, BigDecimal.valueOf(100d));
        bankWithdrawService.withdraw(accountNumber, withdrawAmount);
        assertEquals("Bank account should have a £400 balance", startBalance.minus(withdrawAmount), getBankBalanceFromDb(accountNumber));
    }


    private String createAccount(Money startBalance) {
        String accountNumber = accountCreationService.createAccount(CurrencyUnit.GBP);
        bankDepositService.deposit(accountNumber, startBalance);
        return accountNumber;
    }

    private Money getBankBalanceFromDb(String accountNumber) {
        return bankAccountDao.findById(accountNumber).orElseThrow(() -> new IllegalStateException("Expected account to be found.")).getMoneyBalance();
    }
}
