package org.banking.bankalot.account;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestBankDepositService {

    @Autowired
    private BankAccountDao bankAccountDao;

    @Autowired
    private BankDepositService bankDepositService;


    @Test
    public void testDeposit() {
        String accountNumber = createAccount();
        Money depositAmount = Money.of(CurrencyUnit.GBP, BigDecimal.valueOf(500d));
        bankDepositService.deposit(accountNumber, depositAmount);
        assertEquals("Bank account should have a £500 balance", depositAmount, getBankBalanceFromDb(accountNumber));
    }


    private String createAccount() {
        AccountCreationService service = new AccountCreationService(bankAccountDao);
        return service.createAccount(CurrencyUnit.GBP);
    }

    private Money getBankBalanceFromDb(String accountNumber) {
        return bankAccountDao.findById(accountNumber).orElseThrow(() -> new IllegalStateException("Expected account to be found.")).getMoneyBalance();
    }
}
