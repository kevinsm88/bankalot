package org.banking.bankalot.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TestBankTransferController {

    @Autowired
    private MockMvc mvc;


    @Test
    public void testBankTransfer() throws Exception {
        TransferBean transferBean = new TransferBean();
        transferBean.currency = "GBP";
        transferBean.fromAccountNumber = createAccountWithStartingBalance(Money.of(CurrencyUnit.GBP, BigDecimal.valueOf(500d)));
        transferBean.toAccountNumber = createAccountWithStartingBalance(Money.of(CurrencyUnit.GBP, BigDecimal.valueOf(250d)));
        transferBean.amount = BigDecimal.valueOf(100d);

        String jsonContent = new ObjectMapper().writeValueAsString(transferBean);

        this.mvc.perform(post("/transfer")
                .contentType(MediaType.APPLICATION_JSON).content(jsonContent))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
    }

    private String createAccountWithStartingBalance(Money startingBalance) throws Exception {
        String accountNumber = createAccount(startingBalance.getCurrencyUnit().getCode());
        depositAmountIntoAccount(startingBalance, accountNumber);
        return accountNumber;
    }

    private String createAccount(String currencyCode) throws Exception {
        CurrencyBean currencyBean = new CurrencyBean();
        currencyBean.currency = currencyCode;

        String jsonContentForAccountCreation = new ObjectMapper().writeValueAsString(currencyBean);

        return this.mvc.perform(post("/account")
                .contentType(MediaType.APPLICATION_JSON).content(jsonContentForAccountCreation))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

    private void depositAmountIntoAccount(Money startingBalance, String accountNumber) throws Exception {
        DepositBean depositBean = new DepositBean();
        depositBean.amount = startingBalance.getAmount();
        depositBean.currency = startingBalance.getCurrencyUnit().getCode();
        depositBean.accountNumber = accountNumber;
        String jsonContentForDeposit = new ObjectMapper().writeValueAsString(depositBean);

        this.mvc.perform(post("/deposit")
                .contentType(MediaType.APPLICATION_JSON).content(jsonContentForDeposit))
                .andExpect(status().isOk());
    }
}
