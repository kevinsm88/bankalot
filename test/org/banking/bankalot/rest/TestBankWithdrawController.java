package org.banking.bankalot.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TestBankWithdrawController {

    @Autowired
    private MockMvc mvc;


    @Test
    public void testBankWithdraw() throws Exception {
        Money startBalance = Money.of(CurrencyUnit.GBP, BigDecimal.valueOf(500d));
        String accountNumber = createAccountWithInitialBalance(startBalance);

        DepositBean depositBean = new DepositBean();
        depositBean.accountNumber = accountNumber;
        depositBean.currency = startBalance.getCurrencyUnit().getCode();
        depositBean.amount = BigDecimal.valueOf(250d);

        String jsonContent = new ObjectMapper().writeValueAsString(depositBean);

        this.mvc.perform(post("/withdraw")
                .contentType(MediaType.APPLICATION_JSON).content(jsonContent))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
    }

    private String createAccountWithInitialBalance(Money startBalance) throws Exception {
        DepositBean depositBean = new DepositBean();
        depositBean.currency = startBalance.getCurrencyUnit().getCode();
        depositBean.accountNumber = createAccount(depositBean.currency);
        depositBean.amount = BigDecimal.valueOf(500d);

        String jsonContent = new ObjectMapper().writeValueAsString(depositBean);

        this.mvc.perform(post("/deposit")
                .contentType(MediaType.APPLICATION_JSON).content(jsonContent))
                .andExpect(status().isOk())
                .andExpect(content().string(""));

        return depositBean.accountNumber;
    }

    private String createAccount(String currency) throws Exception {
        CurrencyBean currencyBean = new CurrencyBean();
        currencyBean.currency = currency;

        String jsonContent = new ObjectMapper().writeValueAsString(currencyBean);

        return this.mvc.perform(post("/account")
                .contentType(MediaType.APPLICATION_JSON).content(jsonContent))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(); // the account number is stored in the content
    }
}
