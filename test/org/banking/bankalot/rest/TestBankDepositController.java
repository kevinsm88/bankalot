package org.banking.bankalot.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TestBankDepositController {

    @Autowired
    private MockMvc mvc;


    @Test
    public void testBankDeposit() throws Exception {
        DepositBean depositBean = new DepositBean();
        depositBean.currency = "GBP";
        depositBean.accountNumber = createAccount(depositBean.currency);
        depositBean.amount = BigDecimal.valueOf(500d);

        String jsonContent = new ObjectMapper().writeValueAsString(depositBean);

        this.mvc.perform(post("/deposit")
                .contentType(MediaType.APPLICATION_JSON).content(jsonContent))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
    }

    private String createAccount(String currency) throws Exception {
        CurrencyBean currencyBean = new CurrencyBean();
        currencyBean.currency = currency;

        String jsonContent = new ObjectMapper().writeValueAsString(currencyBean);

        return this.mvc.perform(post("/account")
                .contentType(MediaType.APPLICATION_JSON).content(jsonContent))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(); // the account number is stored in the content
    }
}
