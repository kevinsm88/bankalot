package org.banking.bankalot.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.banking.bankalot.account.AccountHistory;
import org.banking.bankalot.account.AccountStatement;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TestAccountStatementController {

    @Autowired
    private MockMvc mvc;


    @Test
    public void testAccountStatement() throws Exception {
        String[] accountNumbers = setupAccountsWithStartingBalancesAndPerformTransfer();

        verifyAccountHasBalanceAndCashMovements(accountNumbers[0], BigDecimal.valueOf(400).setScale(2), new BigDecimal[] { BigDecimal.valueOf(500).setScale(2), BigDecimal.valueOf(-100).setScale(2) });
        verifyAccountHasBalanceAndCashMovements(accountNumbers[1], BigDecimal.valueOf(350).setScale(2), new BigDecimal[] { BigDecimal.valueOf(250).setScale(2), BigDecimal.valueOf( 100).setScale(2) });
    }

    private void verifyAccountHasBalanceAndCashMovements(String accountNumber, BigDecimal currentBalance, BigDecimal[] transactions) throws Exception {
        AccountStatementBean accountStatementBean = new AccountStatementBean();
        accountStatementBean.accountNumber = accountNumber;

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonContent = objectMapper.writeValueAsString(accountStatementBean);

        String content = this.mvc.perform(get("/account")
                .contentType(MediaType.APPLICATION_JSON).content(jsonContent))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        AccountStatement accountStatement = objectMapper.readValue(content, AccountStatement.class);
        assertEquals(String.format("Balance of the from account should be £%d", currentBalance.intValue()), currentBalance, accountStatement.getBalance());
        assertEquals(String.format("There should be %d transactions", transactions.length), transactions.length, accountStatement.getAccountHistories().size());

        Iterator<AccountHistory> iterator = accountStatement.getAccountHistories().iterator();
        Arrays.stream(transactions).forEach(cashMovement -> assertEquals(String.format("Cash movement should be £%d", cashMovement.intValue()), cashMovement, iterator.next().getCashMovement()));
    }

    private String[] setupAccountsWithStartingBalancesAndPerformTransfer() throws Exception {
        TransferBean transferBean = new TransferBean();
        transferBean.currency = "GBP";
        transferBean.fromAccountNumber = createAccountWithStartingBalance(Money.of(CurrencyUnit.GBP, BigDecimal.valueOf(500)));
        transferBean.toAccountNumber = createAccountWithStartingBalance(Money.of(CurrencyUnit.GBP, BigDecimal.valueOf(250)));
        transferBean.amount = BigDecimal.valueOf(100);

        String jsonContent = new ObjectMapper().writeValueAsString(transferBean);

        this.mvc.perform(post("/transfer")
                .contentType(MediaType.APPLICATION_JSON).content(jsonContent))
                .andExpect(status().isOk())
                .andExpect(content().string(""));

        return new String[] { transferBean.fromAccountNumber, transferBean.toAccountNumber };
    }

    private String createAccountWithStartingBalance(Money startingBalance) throws Exception {
        String accountNumber = createAccount(startingBalance.getCurrencyUnit().getCode());
        depositAmountIntoAccount(startingBalance, accountNumber);
        return accountNumber;
    }

    private String createAccount(String currencyCode) throws Exception {
        CurrencyBean currencyBean = new CurrencyBean();
        currencyBean.currency = currencyCode;

        String jsonContentForAccountCreation = new ObjectMapper().writeValueAsString(currencyBean);

        return this.mvc.perform(post("/account")
                .contentType(MediaType.APPLICATION_JSON).content(jsonContentForAccountCreation))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

    private void depositAmountIntoAccount(Money startingBalance, String accountNumber) throws Exception {
        DepositBean depositBean = new DepositBean();
        depositBean.amount = startingBalance.getAmount();
        depositBean.currency = startingBalance.getCurrencyUnit().getCode();
        depositBean.accountNumber = accountNumber;
        String jsonContentForDeposit = new ObjectMapper().writeValueAsString(depositBean);

        this.mvc.perform(post("/deposit")
                .contentType(MediaType.APPLICATION_JSON).content(jsonContentForDeposit))
                .andExpect(status().isOk());
    }
}
